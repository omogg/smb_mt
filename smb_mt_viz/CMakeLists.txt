cmake_minimum_required(VERSION 2.8.3)
project(smb_mt_viz)


## Use C++11
add_definitions(--std=c++11)

## Find catkin macros and libraries
find_package(catkin REQUIRED COMPONENTS
  smb_mt_description
  joint_state_publisher
  robot_state_publisher
  rviz
  rviz_imu_plugin
)

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if you package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
#  INCLUDE_DIRS
#    include
#  LIBRARIES
  CATKIN_DEPENDS 
  smb_mt_description
  joint_state_publisher
  robot_state_publisher
  rviz
  rviz_imu_plugin
#  DEPENDS 
)


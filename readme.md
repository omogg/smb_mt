# smb_mt

## Overview

This repository includes packages for the simulation of Super Mega Bot (SMB) and a mapping and navigation pipeline.

## Install instruction

The easiest way to get `smb_mt` running is to use the provided virtual machine image. All necessary dependencies are already pre-installed on the image. Instructions on how to set up the virtual machine can be found in the `doc` folder.  

Alternatively, there is a zip file in  `doc` with multiple shell scripts that can help speed up the installation process. Further instructions can be found in the zip file.  

## Packages

The repository includes the following packages:  

* **smb_mt_control:** Contains the control configuration, most importantly the [differential drive controller](http://wiki.ros.org/diff_drive_controller) and the [robot_localization](https://github.com/cra-ros-pkg/robot_localization) package.  
* **smb_mt_description:** The robot description is in this package. This includes visual models of the robot as well as the sensor setup.  
* **smb_mt_gazebo:** This package allows to simulate the robot in gazebo. It also offers multiple gazebo worlds to choose from.  
* **smb_mt_highlevel_controller:** This package ties together all the above mentioned packages and performs various message transformations.  
* **smb_mt_navigation:** A collection of navigation and mapping options are assembled in this package.  
* **smb_mt_viz:** Used for visualization in rviz.  

Note: A large part of this repository is based on the [Husky](https://github.com/husky/husky) packages.

## How to use smb_mt

A tutorial on how to use the packages can be found in the `doc` folder.  

Note: If you are using the `summer_school` branch it is advised to follow the tutorial since some parts of the code are commented out.  



Here is a brief overview on how to run the different packages (branch:summer_school):  

Start the simulation and the state estimation:  
```asm
$ roslaunch smb_mt_highlevel_controller smb_full.launch
```
The configuration of the state estimation can be found in /smb_mt_control/config/localization.yaml.  


In order to navigate there are multiple options. They all build on the [ros navigation stack](http://wiki.ros.org/navigation).

Mapping and navigation can be visualized with the following command:  
```asm
$ roslaunch smb_mt_viz view_robot.launch
```
The easiest way to set a global goal for the robot is to use the `2D Nav Goal` button in Rviz.  

Here are some options for navigation:  

* 2D navigation:  
```asm
$ roslaunch smb_mt_navigation smb_move_base_navigation_2d.launch
```

![alt_text](doc/2d_navigation_4x.gif)  
Navigation in 2d. Ramp is treated as an obstacle.

* navigation with pre-filtered obstacles (allows to navigate in uneven terrain):
```asm
$ roslaunch smb_mt_navigation smb_rtabmap_obstacle_detection.launch
```
```asm
$ roslaunch smb_mt_navigation smb_move_base_obstacle_detection.launch
```

![alt_text](doc/navigation_obstacles_detection.gif)  
Ramp is now accessible due to pre-filtering.

* navigation with elevation mapping:
```asm
$ roslaunch smb_mt_navigation smb_elevation_mapping.launch
```
```asm
$ roslaunch smb_mt_navigation smb_traversability_estimation.launch
```
```asm
$ roslaunch smb_mt_navigation smb_move_base_traversability.launch
```

![alt_text](doc/ELMAP.gif)  
Shows the elevation map, the traversability map (height of 1 is (red) fully traversable, 0 is not traversable) and the corresponding occupancy grid with an inflation layer.


#pragma once

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <std_srvs/SetBool.h>
#include <nav_msgs/Odometry.h>
#include <grid_map_msgs/GridMap.h>
#include <grid_map_ros/grid_map_ros.hpp>
#include <grid_map_ros/GridMapRosConverter.hpp>
#include <tf/transform_listener.h>
#include <pcl_ros/transforms.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/common/io.h>


namespace smb_highlevel_controller {

/*!
 * Class containing the SMB Highlevel Controller
 */
class SMBHighlevelController {
public:
	/*!
	 * Constructor.
	 */
	SMBHighlevelController(ros::NodeHandle& nodeHandle);

	/*!
	 * Destructor.
	 */
	virtual ~SMBHighlevelController();


private:
	ros::NodeHandle nodeHandle_;

	ros::Subscriber subscriber_odometry_filtered_;
	ros::Subscriber subscriber_traversability_map_;
	ros::Subscriber subscriber_odometry_icp_;
	ros::Subscriber subscriber_lidar_pointcloud_;
	ros::Subscriber subscriber_camera_pointcloud_;

	ros::Publisher publisher_odometry_filtered_pose_;
	ros::Publisher publisher_traversability_map_;
	ros::Publisher publisher_odometry_icp_corrected_;
	ros::Publisher publisher_merged_pointcloud_;

	tf::TransformListener listener_pose_icp_corrected_;
	tf::TransformListener listener_lidar_to_base_link_transform_;
	tf::TransformListener listener_pointcloud_;

	bool robot_running_;

	std::string topic_cmd_vel_;
	std::string topic_odometry_filtered_;
	std::string topic_odometry_filtered_pose_;
  std::string topic_traversability_gridMap_;
  std::string topic_traversability_occupancyGrid_;
  std::string topic_sub_icp_odometry_;
  std::string topic_pub_icp_odometry_;
  std::string topic_lidar_pointcloud_;
  std::string topic_camera_pointcloud_;
  std::string topic_merged_pointcloud_;
	int queue_cmd_vel_;
	int queue_odometry_filtered_;
	int queue_odometry_filtered_pose_;
  int queue_traversability_gridMap_;
  int queue_traversability_occupancyGrid_;
  int queue_sub_icp_odometry_;
  int queue_pub_icp_odometry_;
  int queue_lidar_pointcloud_;
  int queue_camera_pointcloud_;
  int queue_merged_pointcloud_;
	double gain_cmd_vel_;
	float vel_x_;

	sensor_msgs::PointCloud2 pointcloud_lidar_;
	sensor_msgs::PointCloud2 pointcloud_camera_;
	sensor_msgs::PointCloud2 pointcloud_merged_;

	bool occupancyGrid_trinary_;
	double p_threshold_;

	std::string icp_version_;

	void get_param();

	void init();

	nav_msgs::Odometry get_icp_corrected_odometry();

	void Callback_odometry_filtered(const nav_msgs::Odometry::ConstPtr& msg);
	void Callback_traversability_conversion(const grid_map_msgs::GridMap::ConstPtr& msg);
	void Callback_icp_libpointmatcher(const nav_msgs::Odometry::ConstPtr& msg);
	void Callback_lidar_pointcloud(const sensor_msgs::PointCloud2::ConstPtr& msg);
	void Callback_camera_pointcloud(const sensor_msgs::PointCloud2::ConstPtr& msg);

	void mergePointClouds();

};

} /* namespace */


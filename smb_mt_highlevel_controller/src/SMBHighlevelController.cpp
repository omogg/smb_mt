#include "smb_mt_highlevel_controller/SMBHighlevelController.hpp"

namespace smb_highlevel_controller {

SMBHighlevelController::SMBHighlevelController(ros::NodeHandle& nodeHandle) :
  nodeHandle_(nodeHandle), robot_running_(false)
{
	SMBHighlevelController::get_param();

	SMBHighlevelController::init();

}

SMBHighlevelController::~SMBHighlevelController()
{
}

void SMBHighlevelController::get_param(){

	nodeHandle_.getParam("odometry/topic_pub",topic_odometry_filtered_pose_);
	nodeHandle_.getParam("odometry/queue_pub",queue_odometry_filtered_pose_);
	nodeHandle_.getParam("odometry/topic_sub",topic_odometry_filtered_);
	nodeHandle_.getParam("odometry/queue_sub",queue_odometry_filtered_);
  nodeHandle_.getParam("traversability/topic_gridMap",topic_traversability_gridMap_);
  nodeHandle_.getParam("traversability/queue_gridMap",queue_traversability_gridMap_);
  nodeHandle_.getParam("traversability/topic_occupancyGrid",topic_traversability_occupancyGrid_);
  nodeHandle_.getParam("traversability/queue_occupancyGrid",queue_traversability_occupancyGrid_);
  nodeHandle_.getParam("pointcloud/topic_lidar",topic_lidar_pointcloud_);
  nodeHandle_.getParam("pointcloud/queue_lidar",queue_lidar_pointcloud_);
  nodeHandle_.getParam("pointcloud/topic_camera",topic_camera_pointcloud_);
  nodeHandle_.getParam("pointcloud/queue_camera",queue_camera_pointcloud_);
  nodeHandle_.getParam("pointcloud/topic_merged",topic_merged_pointcloud_);
  nodeHandle_.getParam("pointcloud/queue_merged",queue_merged_pointcloud_);

  nodeHandle_.getParam("odometry/icp_version",icp_version_);
  if (icp_version_ == "voxblox"){
    nodeHandle_.getParam("odometry/topic_pub_icp_voxblox",topic_pub_icp_odometry_);
    nodeHandle_.getParam("odometry/queue_pub_icp_voxblox",queue_pub_icp_odometry_);
  }
  else if (icp_version_ == "libpointmatcher"){
    nodeHandle_.getParam("odometry/topic_sub_icp_libpointmatcher",topic_sub_icp_odometry_);
    nodeHandle_.getParam("odometry/queue_sub_icp_libpointmatcher",queue_sub_icp_odometry_);
    nodeHandle_.getParam("odometry/topic_pub_icp_libpointmatcher",topic_pub_icp_odometry_);
    nodeHandle_.getParam("odometry/queue_pub_icp_libpointmatcher",queue_pub_icp_odometry_);
  }


  nodeHandle_.getParam("traversability/trinary",occupancyGrid_trinary_);
  nodeHandle_.getParam("traversability/threshold_trinary",p_threshold_);

}

void SMBHighlevelController::init(){

	publisher_odometry_filtered_pose_ = nodeHandle_.advertise<geometry_msgs::PoseWithCovarianceStamped>(topic_odometry_filtered_pose_,queue_odometry_filtered_pose_);
	publisher_traversability_map_ = nodeHandle_.advertise<nav_msgs::OccupancyGrid>(topic_traversability_occupancyGrid_,queue_traversability_occupancyGrid_);
	publisher_merged_pointcloud_ = nodeHandle_.advertise<sensor_msgs::PointCloud2>(topic_merged_pointcloud_,queue_merged_pointcloud_);
	if (icp_version_ == "voxblox"){
	  publisher_odometry_icp_corrected_ = nodeHandle_.advertise<nav_msgs::Odometry>(topic_pub_icp_odometry_,queue_pub_icp_odometry_);
	}
	else if (icp_version_ == "libpointmatcher"){
	  publisher_odometry_icp_corrected_ = nodeHandle_.advertise<nav_msgs::Odometry>(topic_pub_icp_odometry_,queue_pub_icp_odometry_);
	  subscriber_odometry_icp_ = nodeHandle_.subscribe(topic_sub_icp_odometry_, queue_pub_icp_odometry_, &SMBHighlevelController::Callback_icp_libpointmatcher,this);
	}

	subscriber_odometry_filtered_ = nodeHandle_.subscribe(topic_odometry_filtered_, queue_odometry_filtered_,&SMBHighlevelController::Callback_odometry_filtered,this);
	subscriber_traversability_map_ = nodeHandle_.subscribe(topic_traversability_gridMap_,queue_traversability_gridMap_,&SMBHighlevelController::Callback_traversability_conversion,this);
	subscriber_lidar_pointcloud_ = nodeHandle_.subscribe(topic_lidar_pointcloud_,queue_lidar_pointcloud_,&SMBHighlevelController::Callback_lidar_pointcloud,this);
	subscriber_camera_pointcloud_ = nodeHandle_.subscribe(topic_camera_pointcloud_,queue_camera_pointcloud_,&SMBHighlevelController::Callback_camera_pointcloud,this);
}

nav_msgs::Odometry SMBHighlevelController::get_icp_corrected_odometry(){
  tf::StampedTransform transform_icp;
  tf::StampedTransform transform_lidar_base;

  nav_msgs::Odometry odom_icp;
  try{
	listener_pose_icp_corrected_.waitForTransform("/odom","/pose_corrected", ros::Time(), ros::Duration(1.0));
    listener_pose_icp_corrected_.lookupTransform("/odom", "/pose_corrected",
                             ros::Time(0), transform_icp);
    listener_lidar_to_base_link_transform_.lookupTransform("/base_link","/rs_lidar",ros::Time(0),transform_lidar_base);
  }
  catch (tf::TransformException &ex) {
    ROS_ERROR("%s",ex.what());
  }

  nav_msgs::Odometry position_lidar_base;

  odom_icp.header.frame_id = transform_icp.frame_id_;
  odom_icp.header.stamp = transform_icp.stamp_;
  odom_icp.child_frame_id = "base_link";


  tf::poseTFToMsg(transform_icp,odom_icp.pose.pose);
  tf::poseTFToMsg(transform_lidar_base,position_lidar_base.pose.pose);

  //tf message is for lidar frame but you want the base_link (make sure orientation of both frames match):
  odom_icp.pose.pose.position.x = odom_icp.pose.pose.position.x + position_lidar_base.pose.pose.position.x;
  odom_icp.pose.pose.position.y = odom_icp.pose.pose.position.y + position_lidar_base.pose.pose.position.y;
  odom_icp.pose.pose.position.z = odom_icp.pose.pose.position.z - position_lidar_base.pose.pose.position.z;

  //tf doesn't provide covariances (and no twist either) so they have been set manually for now:
  odom_icp.pose.covariance = {0.001, 0, 0, 0, 0, 0,
      0, 0.001, 0, 0, 0, 0,
      0, 0, 0.001, 0, 0, 0,
      0, 0, 0, 0.001, 0, 0,
      0, 0, 0, 0, 0.001, 0,
      0, 0, 0, 0, 0, 0.001};

  publisher_odometry_icp_corrected_.publish(odom_icp);

  return odom_icp;
}

void SMBHighlevelController::Callback_odometry_filtered(const nav_msgs::Odometry::ConstPtr& msg){

	nav_msgs::Odometry a;

	//TEST: odometry von icp verwenden.
	if (icp_version_ == "voxblox"){
	  a = SMBHighlevelController::get_icp_corrected_odometry();
	}
	else{
	  a = *msg;
	}
	geometry_msgs::PoseWithCovarianceStamped msg_pose;
	msg_pose.header = a.header;
	msg_pose.pose = a.pose;

	publisher_odometry_filtered_pose_.publish(msg_pose);
}

void SMBHighlevelController::Callback_icp_libpointmatcher(const nav_msgs::Odometry::ConstPtr& msg){

  nav_msgs::Odometry a = *msg;

  a.header.frame_id = "odom";
  a.child_frame_id = "base_link";

  //no given covariances (and no twist either) so they have been set manually for now:

  a.pose.covariance = {0.001, 0, 0, 0, 0, 0,
      0, 0.001, 0, 0, 0, 0,
      0, 0, 0.001, 0, 0, 0,
      0, 0, 0, 0.001, 0, 0,
      0, 0, 0, 0, 0.001, 0,
      0, 0, 0, 0, 0, 0.001};

  publisher_odometry_icp_corrected_.publish(a);
}

void SMBHighlevelController::Callback_lidar_pointcloud(const sensor_msgs::PointCloud2::ConstPtr& msg){

  pointcloud_lidar_ = *msg;
}

void SMBHighlevelController::Callback_camera_pointcloud(const sensor_msgs::PointCloud2::ConstPtr& msg){

  pointcloud_camera_ = *msg;
  mergePointClouds();
}

void SMBHighlevelController::mergePointClouds(){

  sensor_msgs::PointCloud2 pointcloud_camera_in_lidar_frame;

  bool merge_ok;

  merge_ok = listener_pointcloud_.waitForTransform(pointcloud_lidar_.header.frame_id,pointcloud_camera_.header.frame_id,pointcloud_camera_.header.stamp,ros::Duration(0.2));

  if (merge_ok){
  pcl_ros::transformPointCloud(pointcloud_lidar_.header.frame_id,pointcloud_camera_,pointcloud_camera_in_lidar_frame,listener_pointcloud_);

  pcl::PCLPointCloud2 pcl_lidar;
  pcl::PCLPointCloud2 pcl_camera;
  pcl::PCLPointCloud2 pcl_merged;

  pcl_conversions::toPCL(pointcloud_lidar_,pcl_lidar);
  pcl_conversions::toPCL(pointcloud_camera_in_lidar_frame,pcl_camera);

  pcl::PointCloud<pcl::PointXYZ> p_lidar;
  pcl::PointCloud<pcl::PointXYZ> p_camera;

  pcl::fromPCLPointCloud2(pcl_lidar,p_lidar);
  pcl::fromPCLPointCloud2(pcl_camera,p_camera);

  pcl::toPCLPointCloud2(p_lidar,pcl_lidar);
  pcl::toPCLPointCloud2(p_camera,pcl_camera);

  pcl::concatenatePointCloud(pcl_lidar,pcl_camera,pcl_merged);

  pcl_conversions::fromPCL(pcl_merged,pointcloud_merged_);

  publisher_merged_pointcloud_.publish(pointcloud_merged_);
  }
}

void SMBHighlevelController::Callback_traversability_conversion(const grid_map_msgs::GridMap::ConstPtr& msg){
  grid_map::GridMap a;
  grid_map::GridMapRosConverter::fromMessage(*msg,a);

  for (grid_map::GridMapIterator iterator(a); !iterator.isPastEnd(); ++iterator) {
    if (std::isnan(a.at("traversability", *iterator)) == false){
    a.at("traversability", *iterator) = 1.0-a.at("traversability", *iterator);
    //in case of trinary distinction (occupied / free / unknown)
    if (occupancyGrid_trinary_)
    {
      if (a.at("traversability", *iterator) >= p_threshold_){
        a.at("traversability", *iterator) = 1.0;
      }
      else{
        a.at("traversability", *iterator) = 0.0;
      }
    }
    }
  }

  nav_msgs::OccupancyGrid occupancyGridResult;
  grid_map::GridMapRosConverter::toOccupancyGrid(a, "traversability", 0.0, 1.0, occupancyGridResult);
  publisher_traversability_map_.publish(occupancyGridResult);
}

} /* namespace */

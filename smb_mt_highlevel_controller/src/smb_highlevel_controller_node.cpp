#include <ros/ros.h>
#include "smb_mt_highlevel_controller/SMBHighlevelController.hpp"

int main(int argc, char** argv)
{
  ros::init(argc, argv, "smb_highlevel_controller");
  ros::NodeHandle nodeHandle("~");

  smb_highlevel_controller::SMBHighlevelController smbHighlevelController(nodeHandle);

  ros::spin();
  return 0;
}
